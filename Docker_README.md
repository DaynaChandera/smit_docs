Docker and docker-compose

Docker:
    - Lightweight virtualmachine like technology with linux kernel
    - Build Images
    - Have to Define "Dockerfile" as mentioned without any extension.
    - Can operate on single image/ it can build only single image of specified application

Compose file:
    - Dockerfile is limited to one image with one container. So compose file is used to spin up multiple containers(e.g.: main_app, mysql, redis, adminer etc.)
    - define docker-compose.yaml
    - Dockerfile can be referenced in compose file with "context" attribute
    - topics that are must to be learned for efficient configuration: volumes, networks, command, resources
    - two most used command for the most cases: => docker-compose build --no-cache
                                                => docker-compose up

# For pulling or pushing custom images on the server or on local machine "docker login" is necessary.
# specifying docker images in compose file for all containers reduces burden of pulling or pushing of whole application code to the server. only docker-compose file is needed.
# creating and pushing images to docker hub also helps in development with kubernetes.

# Some commands to terminate and delete running docker images and resources.:
  - stop all containers:
       > docker kill $(docker ps -q)
  - remove all containers:
       > docker rm $(docker ps -a -q)
  - remove all docker images:
       > docker rmi $(docker images -q)

  - remove resources:
       > docker system prune
